# firebase-pwa

This is a super simple hello world progressive web app (PWA) deployed with Firebase.

It includes push notification, which isn't required in a PWA, but it's cool.
Check out the video here: [https://www.youtube.com/watch?time_continue=1&v=BsCBCudx58g](https://www.youtube.com/watch?time_continue=1&v=BsCBCudx58g)


Check out the hosted version here: [https://build-pwa-example.firebaseapp.com/](https://build-pwa-example.firebaseapp.com/)
