// Firebase Auth
var provider = new firebase.auth.GoogleAuthProvider();

let loader = window.localStorage.getItem("loader");

// If its go for login this will activated.
if (loader === "on") {
  document.getElementById("screen").setAttribute("class", "screen");
}

function loginGoogle () {
    window.localStorage.setItem("loader", "on");
    firebase.auth().signInWithRedirect(provider).then(function(result) {
      log("come on ");
        // This gives you a Google Access Token. You can use it to access the Google API.
        var token = result.credential.accessToken;
        // The signed-in user info.
        var user = result.user;
    }).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // The email of the user's account used.
        var email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential;
    });
}

firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    if (firebase.auth().currentUser){
        window.location.href = "/circle.html";
        window.localStorage.setItem("loader", "off");
    }
  } else {
      window.localStorage.setItem("loader", "off");
  }
});
