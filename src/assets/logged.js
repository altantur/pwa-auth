firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    if (firebase.auth().currentUser)
        console.log("Logged in");
  } else {
        window.location.href = "/";   
  }
});

function logout () {
    firebase.auth().signOut();
}

