// Initialize Firebase
var firebaseConfig = {
    apiKey: "AIzaSyBRck_x5HkL5tDqP0TBvGqegRlQSVAQe7Y",
    authDomain: "build-pwa-example.firebaseapp.com",
    databaseURL: "https://build-pwa-example.firebaseio.com",
    projectId: "build-pwa-example",
    storageBucket: "build-pwa-example.appspot.com",
    messagingSenderId: "206351260691",
    appId: "1:206351260691:web:d1706aacd8e6687d"
};
firebase.initializeApp(firebaseConfig);

// Service Worker
// if ('serviceWorker' in navigator) {
//     navigator.serviceWorker.register('/sw.js')
//     .then(function () {
//         console.log('Service Worker Registered');
//     });
// }
// Push notification
const database = firebase.database();
const messaging = firebase.messaging();

messaging.requestPermission()
.then(function () {
    console.log('Notification permission granted.');
    return messaging.getToken();
})
.then(function (token) {
    console.log(token);
    database.ref("tokens").set({token: token});
})
.catch(function (err) {
    console.log('Unable to get permission to notify.', err);
})