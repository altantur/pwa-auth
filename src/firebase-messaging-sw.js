importScripts('https://www.gstatic.com/firebasejs/4.4.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.4.0/firebase-messaging.js');

var firebaseConfig = {
	apiKey: "AIzaSyBRck_x5HkL5tDqP0TBvGqegRlQSVAQe7Y",
	authDomain: "build-pwa-example.firebaseapp.com",
	databaseURL: "https://build-pwa-example.firebaseio.com",
	projectId: "build-pwa-example",
	storageBucket: "build-pwa-example.appspot.com",
	messagingSenderId: "206351260691",
	appId: "1:206351260691:web:d1706aacd8e6687d"
  };
firebase.initializeApp(firebaseConfig);

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
	const title = 'Hello World';
	const options = {
		body: payload.data.body
	};
	return self.registration.showNotification(title, options);
});